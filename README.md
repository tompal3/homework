# homework

Use *Vagrant boxes* and install new windows vm. Use this box for your setup:
https://app.vagrantup.com/StefanScherer/boxes/windows_2019

## Goal

The initial goal is to create new box and automate vm startup using powershell.
Configuration should be stored in .json file as provided in example [config.json](https://gitlab.com/tompal3/homework/-/blob/master/config.json). 
Use github or gitlab for all your automation scripts.

## Requirements

Configuration should include:
- Setup server hostname (*hostname* in *config.json*). Hostname should match pattern 10 charachters and 3 integers at the end like *fileserver001*
- Setup server DNS'es (*dns1* and *dns2* in *config.json*). Ips should be validated for type errors.
- Environment is equivalent to Workgroup name in this exercise. Allowed values are STAGING and PRODUCTION
- Server role represents witch windows role should be installed. Allowed values *fileserver* or *webserver*.:
    - fileserver: should create new share with one file named helloworld.txt 
    - webserver: should host "helloworld" on http port 80 
- A new local user created for *Owner* field in config file. Set its password to "vagrant" and add it to local Administrators group.
- Server should have chocolatey installed and a list of packages in config file should be installed.
- Firewall configuration should accept "disabled" or "enabled" and RDP and WINRM protocols should be configured to be allowed for all profiles.

All errors should be handled and written to the log file.
If validation of any configuration fails use default values for it. 
